import styled from 'styled-components'
import media from 'styled-media-query'

export const FormWrapper = styled.form`
    display: flex;
    width: 600px;
    max-width: 600px;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding-top: 5%;

    ${media.lessThan('medium')`
        max-width: 300px;
        margin-top: 30px;
    `}
`

export const FormText = styled.h1`
    text-align: center;
    margin-bottom: 20px;
    font-size: 36px;
    line-height: 44px;
    max-width: 500px;
    color: var(--font-black);
    font-family: 'Roboto';
    
    b {
        font-weight: bolder;
    }
`

export const Button = styled.button`
    min-width: 150px;
    margin-top: 10px;
    background-color: var(--bg-yellow);
    border-radius: 24px;
    padding: 12px;
    text-transform: uppercase;
    font-size: 14px;
    border: 0px;
    opacity: 0.9;
    cursor: pointer;

    &:hover {
        opacity: 1;
    }
`