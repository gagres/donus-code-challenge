import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { geocodeRequestAction } from '../../ducks/geocode'
import { addNotification } from '../../ducks/notification'

import Content from '../../components/Content'
import Search from '../../components/Search'

import * as S from './styled'

const HomeContainer = ({ geocodeRequest }) => {
    const [address, setAddress] = useState('')
    function searchAddress(e) {
        e.preventDefault()
        geocodeRequest(address)
    }

    return (
        <S.FormWrapper
            onSubmit={searchAddress}>
            <S.FormText>
                <b>Bebidas geladas</b> a <b>preço</b> de mercado na sua casa <b>agora</b>
            </S.FormText>
            <Search value={address} 
                onChange={(e) => setAddress(e.target.value)}/>
            <S.Button>Pesquisar</S.Button>
        </S.FormWrapper>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        geocodeRequest: (address) => dispatch(geocodeRequestAction(address)),
    }
}

HomeContainer.propTypes = {
    requestError: PropTypes.object,
}

export default connect(null, mapDispatchToProps)(HomeContainer)