import { createGlobalStyle } from 'styled-components'

import robotoRegular from './assets/fonts/Roboto-Regular.ttf';
import robotoBlack from './assets/fonts/Roboto-Black.ttf';
import robotoThin from './assets/fonts/Roboto-Thin.ttf';
import robotoMedium from './assets/fonts/Roboto-Medium.ttf';

export const GlobalStyling = createGlobalStyle`
    @font-face {
        font-family: "Roboto";
        src: url(${robotoRegular})
    }

    @font-face {
        font-family: "Roboto Black";
        src: url(${robotoBlack})
    }

    @font-face {
        font-family: "Roboto Thin";
        src: url(${robotoThin})
    }

    @font-face {
        font-family: "Roboto Medium";
        src: url(${robotoMedium})
    }

    /* http://meyerweb.com/eric/tools/css/reset/ 
    v2.0 | 20110126
    License: none (public domain)
    */

   html, body, div, span, applet, object, iframe,
    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
    a, abbr, acronym, address, big, cite, code,
    del, dfn, em, img, ins, kbd, q, s, samp,
    small, strike, strong, sub, sup, tt, var,
    b, u, i, center,
    dl, dt, dd, ol, ul, li,
    fieldset, form, label, legend,
    table, caption, tbody, tfoot, thead, tr, th, td,
    article, aside, canvas, details, embed, 
    figure, figcaption, footer, header, hgroup, 
    menu, nav, output, ruby, section, summary,
    time, mark, audio, video {
        margin: 0;
        padding: 0;
        border: 0;
        font-size: 100%;
        font: inherit;
        font-family: "Roboto";
        vertical-align: baseline;
    }
    /* HTML5 display-role reset for older browsers */
    article, aside, details, figcaption, figure, 
    footer, header, hgroup, menu, nav, section {
        display: block;
    }
    body {
        line-height: 1;
        background-color: rgb(245, 245, 245);
    }
    ol, ul {
        list-style: none;
    }
    blockquote, q {
        quotes: none;
    }
    blockquote:before, blockquote:after,
    q:before, q:after {
        content: '';
        content: none;
    }
    table {
        border-collapse: collapse;
        border-spacing: 0;
    }

    textarea:focus, input:focus{
        outline: none;
    }

    /* additions */
    * {
        box-sizing: border-box;
    }

    :root {
        --header-height: 80px;
        --bg-black: #333;
        --bg-yellow: rgb(255, 197, 0);
        --bg-yellow-hover: rgb(249, 193, 4);;
        --bg-white: rgb(255, 255, 255);
        --bg-grey: rgb(245, 245, 245);
        --bg-grey-2: #666;
        --bg-grey-3: rgb(153, 153, 153);
        --bg-grey-4: rgb(230, 230, 230);
        --font-white: rgb(255, 255, 255);
        --font-black: rgb(51, 51, 51);
        --font-grey: #999;
        --font-grey-2: #666;
        --font-grey-3: rgb(153, 153, 153);
        --font-yellow: rgb(255, 197, 0);
    }

    /* Loaders */
    .lds-ring {
        display: inline-block;
        position: relative;
        width: 40px;
        height: 40px;
    }
    .lds-ring div {
        box-sizing: border-box;
        display: block;
        position: absolute;
        width: 40px;
        height: 40px;
        border: 4px solid var(--bg-yellow);
        border-radius: 50%;
        animation: lds-ring 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        border-color: var(--bg-yellow) transparent transparent transparent;
    }
    .lds-ring div:nth-child(1) {
        animation-delay: -0.45s;
    }
    .lds-ring div:nth-child(2) {
        animation-delay: -0.3s;
    }
    .lds-ring div:nth-child(3) {
        animation-delay: -0.15s;
    }
    @keyframes lds-ring {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }
`