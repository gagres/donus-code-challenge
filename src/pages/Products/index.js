import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import RouterPropTypes from 'react-router-prop-types'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

import Content from '../../components/Content'
import ProductList from '../../components/ProductList'
import Message from '../../components/Message'

import * as S from './styled'

const GET_POC_ADDRESS = gql`
    query pocSearchMethod($now: DateTime!, $algorithm: String!, $lat: String!, $long: String!) {
        pocSearch(now: $now, algorithm: $algorithm, lat: $lat, long: $long) {
            __typename
            id
            status
        }
    }
`

const ProductsPage = ({
    address, history, addressCacheLoaded
}) => {
    if (!address) {
        if (addressCacheLoaded) {
            history.push('/');
            return null;
        } else {
            return <label>Carregando endereço..</label>
        }
    }

    const { location } = address;

    return (
        <Content>
            <Query query={GET_POC_ADDRESS}
                variables={{
                    algorithm: 'NEAREST',
                    // lat: -23.632919,
                    // long: -46.699453,
                    lat: location.lat,
                    long: location.lng,
                    now: new Date().toISOString(),
                }}>
                {
                    ({ data, loading, error }) => {
                        if (loading) {
                            return (
                                <Message text="Carregando endereço..."/>
                            )
                        } else if (error) {
                            return (
                                <Message text="Ocorreu algum erro ao buscar o endereço =("
                                    linkTitle="Voltar para o início"
                                    linkHref="/"/>
                            )
                        }

                        if (!data.pocSearch.length) {
                            return (
                                <Message text="Não encontramos nenhum distribuidor para este endereço =]"
                                    linkTitle="Buscar endereços"
                                    linkHref="/"/>
                            )
                        }

                        return (
                            <S.ProductsWrapper>
                                <ProductList pocId={data.pocSearch[0].id}/>
                            </S.ProductsWrapper>
                        )
                    }
                }
            </Query>
        </Content>
    )
}

const mapStateToProps = state => ({
    address: state.geocode.address,
    addressCacheLoaded: state.geocode.cacheLoaded
})

ProductsPage.propTypes = {
    address: PropTypes.object,
    addressCacheLoaded: PropTypes.bool,
    history: RouterPropTypes.history,
}

export default connect(mapStateToProps)(ProductsPage)