import React from 'react'

import Content from '../../components/Content';
import Message from '../../components/Message';

const NotFoundPage = () => {
    return (
        <Content>
            <Message text="A pagina que você está buscando não existe =["
                linkTitle="Buscar produtos"
                linkHref="/" />
        </Content>
    )
}

export default NotFoundPage;