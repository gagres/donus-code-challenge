import styled from 'styled-components'
import media from 'styled-media-query'

export const HomeWrapper = styled.section`
    width: 100%;
    height: auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`

export const HomeSearchSection = styled.section`
    width: 100%;
    height: 450px;
    max-height: 450px;
    display: flex;
    justify-content: center;
    align-items: flex-start;
    background-repeat: no-repeat;
    background-size: auto 500px;
`

export const HowItWorksWrapper = styled.section`
    width: 100%;
    max-width: 1200px;
    display: flex;
    flex-direction: column;
    text-align: center;
    padding-top: 2.5%;

    ${media.lessThan('medium')`
        padding-top: 15%;
    `}
`

export const HowItWorksTitle = styled.h2`
    font-size: 32px;
    font-family: 'Roboto';
    font-weight: 500;
    color: var(--font-black);
`

export const HowItWorksList = styled.div`
    display: flex;
    justify-content: space-between;
    margin-top: 2.5%;

    ${media.lessThan('medium')`
        flex-direction: column;
        align-items: center;
    `}
`

export const HowItWorksItem = styled.div`
    display: flex;
    flex-direction: column;
    max-width: 288px;
    text-align: center;
    align-items: center;
`

export const HowItWorksItemImg = styled.img`
    width: 112px;
`

export const HowItWorksItemTitle = styled.h3`
    line-height: 24px;
    color: var(--font-black);
    font-size: 24px;
`

export const HowItWorksItemText = styled.p`
    color: var(--font-grey);
    font-size: 16px;
    line-height: 32px;
`