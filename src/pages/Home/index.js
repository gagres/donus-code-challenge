import React from 'react'

import Content from '../../components/Content'

import HomeContainer from '../../containers/Home'
import welcomeBg from '../../assets/img/welcome-bg.jpeg'
import plasticBag from '../../assets/img/plastic-bag.png'
import doubleCellphone from '../../assets/img/double_cellphone_greeting.png'
import deliveryMan from '../../assets/img/delivery-man.png'

import * as S from './styled'
import Message from '../../components/Message'

const howItWorks = [
    { title: 'Onde você estiver', img: deliveryMan, description: 'Achamos as bebidas geladinhas na sua área e levamos até você!' },
    { title: 'Só as favoritas', img: plasticBag, description: 'Você pode escolher entre cervejas, vinhos, água, energéticos, refrigerantes, salgadinhos e até gelo!' },
    { title: 'Facilita seu brinde', img: doubleCellphone, description: 'Suas bebidas chegam geladinhas e super rápidas, prontas para brindar!' }
];

const HomePage = () => (
    <Content>
        <S.HomeWrapper>
            <S.HomeSearchSection style={{
                backgroundImage: `url(${welcomeBg})`
            }}>
                <HomeContainer />
            </S.HomeSearchSection>
            <S.HowItWorksWrapper>
                <S.HowItWorksTitle>Como funciona o Zé Delivery?</S.HowItWorksTitle>
                <S.HowItWorksList>
                    {howItWorks.map((data, index) => (
                        <S.HowItWorksItem key={index}>
                            <S.HowItWorksItemImg src={data.img}/>
                            <S.HowItWorksItemTitle>
                                {data.title}
                            </S.HowItWorksItemTitle>
                            <S.HowItWorksItemText>
                                {data.description}
                            </S.HowItWorksItemText>
                        </S.HowItWorksItem>    
                    ))}
                </S.HowItWorksList>
            </S.HowItWorksWrapper>
        </S.HomeWrapper>
    </Content>
)

export default HomePage