const currencyFormatter = new Intl.NumberFormat([], {
    style: 'currency',
    currency: 'BRL'
})

export const formatCurrency = (currency) => {
    return currencyFormatter.format(currency).replace(/^(\D+)/, '$1 ')
}

const datetimeFormatter = new Intl.DateTimeFormat('pt-BR')

export const formatDate = (datetime) => {
    return datetimeFormatter.format(datetime)
}