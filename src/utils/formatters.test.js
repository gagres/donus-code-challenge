import {
    formatCurrency,
    formatDate
} from './formatters'

describe('Formatters Utils', () => {
    test('currency formatter', () => {
        expect(formatCurrency(2.9)).toBe('R$ 2.90')
    })
    test('currency formatter', () => {
        const date = new Date('June 11, 2020 11:13:00');
        expect(formatDate(date)).toBe('6/11/2020')
    })
})
