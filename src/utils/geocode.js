
import axios from '../utils/axios'
import success from './success'

export const STATUS_MESSAGES = {
    'ZERO_RESULTS': 'Não foi possível encontrar o endereço desejado',
    'OVER_QUERY_LIMIT': 'Limíte de requisições para a API de geolocalização atingido',
    'REQUEST_DENIED': 'Acesso negado',
    'INVALID_REQUEST': 'Informações da requisição estão faltando',
    'UNKNOWN_ERROR': 'A requisição falhou, tente novamente'
}

/**
 * search address on google geocode API
 * @param {String} address 
 */
export const geocodeRequest = async (address) => {
    try {
        const { data } = await axios.get(`/maps/api/geocode/json?address=${address}`)
        // const data = success;

        if (!data.status) {
            return { body: { message: 'Formato inválido' } };
        }

        switch(data.status) {
            case 'OK':
                return { body: data.results }
            case 'ZERO_RESULTS':
                return { error: STATUS_MESSAGES[data.status] }
            case 'OVER_QUERY_LIMIT':
                return { error: STATUS_MESSAGES[data.status] }
            case 'REQUEST_DENIED':
                return { error: STATUS_MESSAGES[data.status]}
            case 'INVALID_REQUEST':
                return { error: STATUS_MESSAGES[data.status] }
            case 'UNKNOWN_ERROR':
            default:
                return { error: STATUS_MESSAGES['UNKNOWN_ERROR'] }
        }
    } catch (err) {
        return { error: 'Erro na requisição para a API' }
    }
}