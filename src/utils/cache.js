
/**
 * set item on local storage
 * @param {String} key
 * @param {*} value 
 */
export const setItem = (key, value) => {
    let newValue = value
    if (typeof value === 'object') {
        newValue = JSON.stringify(value)
    }
    window.localStorage.setItem(key, newValue)
}

/**
 * get item from local storage
 * @param {String} key 
 */
export const getItem = (key) => {
    const value = window.localStorage.getItem(key);
    try {
        return JSON.parse(value)
    } catch (_) {
        return value
    }
}

/**
 * clear local storage
 */
export const clear = () => {
    window.localStorage.clear();
}