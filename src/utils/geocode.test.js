import {
    geocodeRequest,
    STATUS_MESSAGES,
} from './geocode'

const mockAxios = jest.genMockFromModule('axios')
// this is the key to fix the axios.create() undefined error!
mockAxios.create = jest.fn(() => mockAxios)

describe('Geocode Utils', () => {
    const address = 'abc';
    test('success request', () => {
        const response = { status: 'OK', results: address }
        mockAxios.get.mockImplementationOnce(() => response);
        geocodeRequest(address)
            .then((result) => {
                expect(result).toEqual({ body: response.results });
            })
            .catch(() => {})
    })

    test('failed request', () => {
        let response = { status: 'ZERO_RESULTS' };
        mockAxios.get.mockImplementationOnce(() => response);
        geocodeRequest(address)
            .then((result) => {
                expect(result).toEqual({ error: STATUS_MESSAGES[response.status] });
            })
            .catch(() => {})

        response = { status: 'OVER_QUERY_LIMIT' };
        mockAxios.get.mockImplementationOnce(() => response);
        geocodeRequest(address)
            .then((result) => {
                expect(result).toEqual({ error: STATUS_MESSAGES[response.status] });
            })
            .catch(() => {})

        response = { status: 'REQUEST_DENIED' };
        mockAxios.get.mockImplementationOnce(() => response);
        geocodeRequest(address)
            .then((result) => {
                expect(result).toEqual({ error: STATUS_MESSAGES[response.status] });
            })
            .catch(() => {})
        
        response = { status: 'INVALID_REQUEST' };
        mockAxios.get.mockImplementationOnce(() => response);
        geocodeRequest(address)
            .then((result) => {
                expect(result).toEqual({ error: STATUS_MESSAGES[response.status] });
            })
            .catch(() => {})
            
        response = { status: 'UNKNOWN_ERROR' };
        mockAxios.get.mockImplementationOnce(() => response);
        geocodeRequest(address)
            .then((result) => {
                expect(result).toEqual({ error: STATUS_MESSAGES[response.status] });
            })
            .catch(() => {})

        
        response = { status: 'abc' };
        mockAxios.get.mockImplementationOnce(() => response);
        geocodeRequest(address)
            .then((result) => {
                expect(result).toEqual({ error: STATUS_MESSAGES['UNKNOWN_ERROR'] });
            })
            .catch(() => {})
    })
})