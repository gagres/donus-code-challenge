export default {
    "results": [
        {
            "address_components": [
                {
                    "long_name": "215",
                    "short_name": "215",
                    "types": [
                        "street_number"
                    ]
                },
                {
                    "long_name": "Rua Tapajós",
                    "short_name": "R. Tapajós",
                    "types": [
                        "route"
                    ]
                },
                {
                    "long_name": "Vila Santa Maria",
                    "short_name": "Vila Santa Maria",
                    "types": [
                        "political",
                        "sublocality",
                        "sublocality_level_1"
                    ]
                },
                {
                    "long_name": "Jundiaí",
                    "short_name": "Jundiaí",
                    "types": [
                        "administrative_area_level_2",
                        "political"
                    ]
                },
                {
                    "long_name": "São Paulo",
                    "short_name": "SP",
                    "types": [
                        "administrative_area_level_1",
                        "political"
                    ]
                },
                {
                    "long_name": "Brasil",
                    "short_name": "BR",
                    "types": [
                        "country",
                        "political"
                    ]
                },
                {
                    "long_name": "13203-236",
                    "short_name": "13203-236",
                    "types": [
                        "postal_code"
                    ]
                }
            ],
            "formatted_address": "R. Tapajós, 215 - Vila Santa Maria, Jundiaí - SP, 13203-236, Brasil",
            "geometry": {
                "location": {
                    "lat": -23.205024,
                    "lng": -46.8551331
                },
                "location_type": "ROOFTOP",
                "viewport": {
                    "northeast": {
                        "lat": -23.2036750197085,
                        "lng": -46.85378411970849
                    },
                    "southwest": {
                        "lat": -23.2063729802915,
                        "lng": -46.85648208029149
                    }
                }
            },
            "place_id": "ChIJz5lJldIgz5QRxh2T0UZXpcE",
            "plus_code": {
                "compound_code": "Q4VV+XW Vila Santa Maria, Jundiaí - SP, Brasil",
                "global_code": "588MQ4VV+XW"
            },
            "types": [
                "street_address"
            ]
        }
    ],
    "status": "OK"
}