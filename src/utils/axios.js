import axios from 'axios'

const axiosObject = axios.create({
    baseURL: 'https://maps.googleapis.com',
})

axiosObject.interceptors.request.use(function (request) {
    const { MAPS_GOOGLE_KEY } = process.env;
    if (request.url.indexOf('?') !== -1) {
        request.url += `&key=${MAPS_GOOGLE_KEY}`
    } else {
        request.url += `?key=${MAPS_GOOGLE_KEY}`
    }
    return request
}, function (err) {
    return Promise.reject(err)
})

export default axiosObject