import reducer, {
    Types,
    addNotification,
    removeNotification,
    clearNotifications
} from './notification';
import { initRequest } from './request';

describe('Notification Actions', () => {
    test('add notification', () => {
        const payload = { message: 'Hello', type: 'success' };
        const result = { type: Types.NOTIFICATION_ADD, payload };
        expect(addNotification(...Object.values(payload))).toEqual(result);
    })

    test('remove notification', () => {
        const result = { type: Types.NOTIFICATION_REMOVE }
        expect(removeNotification()).toEqual(result);
    })

    test('clear notifications', () => {
        const result = { type: Types.NOTIFICATIONS_CLEAR }
        expect(clearNotifications()).toEqual(result);
    })
})

describe('Notification Reducer', () => {
    const payload = { message: 'Hello', type: 'success' };

    test('return init state', () => {
        expect(reducer(undefined, {})).toEqual({ queue: [] })
    })

    test('add notification', () => {
        const initState = { queue: [] }
        const action = {
            type: Types.NOTIFICATION_ADD,
            payload
        }

        const newState = reducer(initState, action);

        expect(newState).toEqual({
            queue: [
                { ...payload, id: 0 }
            ]
        });

        expect(reducer(newState, action)).toEqual({
            queue: [
                ...newState.queue,
                { ...payload, id: 1 }
            ]
        });
    })

    test('remove notification', () => {
        const initState = {
            queue: [
                { ...payload, id: 0 }, 
                { ...payload, id: 1 }
            ]
        }
        const action = { type: Types.NOTIFICATION_REMOVE };

        const newState = reducer(initState, action);

        expect(newState).toEqual({
            queue: initState.queue.slice(1)
        });

        expect(reducer(newState, action)).toEqual({
            queue: []
        });
    })

    test('clear notifications', () => {
        const initState = {
            queue: [
                { ...payload, id: 0 }, 
                { ...payload, id: 1 }
            ]
        }
        const action = { type: Types.NOTIFICATIONS_CLEAR };

        expect(reducer(initState, action)).toEqual({
            queue: []
        })
    })
})