import history from '../utils/history'
import { geocodeRequest } from '../utils/geocode'
import * as localStorage from '../utils/cache'

import {
    initRequest,
    setRequestError,
    setRequestResponse
} from './request'

// Types
export const Types = {
    SAVE_ADDRESS: 'geocode/save/address',
    REQUEST: 'geocode/request',
    ERROR: 'geocode/error',
    CACHE_LOAD: 'geocode/cache/load'
}

// Actions
/**
 * @param {Object} location 
 */
export const saveAddress = (location) => {
    return {
        type: Types.SAVE_ADDRESS,
        payload: location
    }
}

export const cacheLoad = (location) => {
    return {
        type: Types.CACHE_LOAD,
        payload: location
    }
}

export const setError = (message) => {
    return {
        type: Types.ERROR,
        payload: { message }
    }
}

const initialState = {
    address: null,
    isEmpty: true,
    error: null,
    cacheLoaded: false,
}

// Reducer
export default function reducer(state = initialState, action) {
    switch(action.type) {
        case Types.ERROR:
            return { ...state, error: {
                    message: action.payload.message,
                    error: action.payload.error
                }
            }
        case Types.SAVE_ADDRESS:
            return {
                ...state,
                error: null,
                address: action.payload,
                isEmpty: !action.payload
            }
        case Types.CACHE_LOAD:
            return {
                ...state,
                address: action.payload,
                isEmpty: !action.payload,
                cacheLoaded: true
            }
        default:
            return state
    }
}

// Action Creators
/**
 * search address on google geocode API
 * @param {String} address
 */
export function geocodeRequestAction(address) {
    return (dispatch) => {
        dispatch(initRequest())
        return geocodeRequest(address)
            .then((result) => {
                if (result.body) {
                    dispatch(setRequestResponse(result.body));
                    const { geometry: { location } } = result.body[0];
                    const newAddress = { address, location };
                    localStorage.setItem('address', newAddress);
                    dispatch(saveAddress(newAddress));
                    history.push('/produtos');
                } else {
                    dispatch(setRequestError(result.error, result));
                    dispatch(setError(result.error));
                }
            })
    }
}

export function initLocation() {
    return (dispatch, getState) => {
        const {
            geocode: {
                address,
                cacheLoaded
            }
        } = getState();
        if (!address && !cacheLoaded) {
            const address = localStorage.getItem('address');
            dispatch(cacheLoad(address));
        }
    }
}