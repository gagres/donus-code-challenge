// Types
export const Types = {
    NOTIFICATION_ADD: 'notification/add',
    NOTIFICATION_REMOVE: 'notification/remove',
    NOTIFICATIONS_CLEAR: 'notification/clear'
}

// Actions
export const addNotification = (message, type = 'warn') => {
    return { type: Types.NOTIFICATION_ADD, payload: { message, type }}
}

export const removeNotification = () => {
    return { type: Types.NOTIFICATION_REMOVE }
}

export const clearNotifications = () => {
    return { type: Types.NOTIFICATIONS_CLEAR }
}

// Reducer
let counter = 0;
const initialState = {
    queue: []
}

export default function reducer(state = initialState, action) {
    switch(action.type) {
        case Types.NOTIFICATION_ADD:
            return {
                queue: [...state.queue, {
                    ...action.payload,
                    id: counter++
                }]
            }
        case Types.NOTIFICATION_REMOVE:
            return {
                queue: state.queue.slice(1)
            }
        case Types.NOTIFICATIONS_CLEAR:
            return { queue: [] }
        default:
            return state;
    }
}