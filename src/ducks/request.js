// Types
export const Types = {
    REQUEST: 'request/send',
    RESPONSE: 'request/response',
    ERROR: 'request/error'
}

import {
    addNotification
} from './notification'

// Actions
export const initRequest = () => {
    return { type: Types.REQUEST }
}

export const setRequestResponse = (response) => {
    return { type: Types.RESPONSE, payload: response }
}

export const setError = (error) => {
    return { type: Types.ERROR, payload: error }
}

// Reducer
const initialState = {
    loading: false,
    lastResponse: null,
    lastError: null
}

export default function reducer(state = initialState, action) {
    switch(action.type) {
        case Types.REQUEST:
            return { ...state, loading: true }
        case Types.RESPONSE:
            return {
                ...state,
                loading: false,
                lastResponse: action.payload
            }
        case Types.ERROR:
            return {
                ...state,
                loading: false,
                lastError: action.payload
            }
        default:
            return state;
    }
}

// Action Creators
export const setRequestError = (message, errorObj) => {
    return dispatch => {
        dispatch(addNotification(message, 'error'));
        dispatch(setError(errorObj));
    }
}