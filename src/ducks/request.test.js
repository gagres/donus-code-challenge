import reducer, {
    Types,
    setRequestResponse,
    setError,
    setRequestError,
    initRequest
} from './request'
import { addNotification } from './notification'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'

const mockStore = configureStore([thunk]);

describe('Request Actions', () => {
    const payload = 'payload';
    test('set response', () => {
        const response = { type: Types.RESPONSE, payload };
        expect(setRequestResponse(payload)).toEqual(response);
    })
    test('set error', () => {
        const error = { type: Types.ERROR, payload }
        expect(setError(payload)).toEqual(error);
    })
    test('init request', () => {
        const request = { type: Types.REQUEST };
        expect(initRequest()).toEqual(request)
    })
})

describe('Request Reducer', () => {
    test('return init state', () => {
        expect(reducer(undefined, {})).toEqual({
            loading: false,
            lastResponse: null,
            lastError: null
        })
    });

    test('init the request', () => {
        expect(reducer(undefined, {
            type: Types.REQUEST
        })).toEqual({
            loading: true,
            lastResponse: null,
            lastError: null
        })
    })

    test('set error', () => {
        const initialState = {
            loading: true,
            lastResponse: null,
            lastError: null
        }
        const action = { type: Types.ERROR, payload: 'error' };
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            loading: false,
            lastError: action.payload
        })
    })

    test('set response', () => {
        const initialState = {
            loading: true,
            lastResponse: null,
            lastError: null
        }
        const action = { type: Types.RESPONSE, payload: 'response' };
        expect(reducer(initialState, action)).toEqual({
            ...initialState,
            loading: false,
            lastResponse: action.payload
        })
    })
})

describe('Request Action Creators', () => {
    let store;
    beforeEach(() => {
        store = mockStore({});
    })

    test('set request error', () => {
        const errorObj = { message: 'error' };
        store.dispatch(setRequestError(errorObj.message, errorObj));

        const actions = store.getActions();
        
        expect(actions[0]).toEqual(addNotification(errorObj.message, 'error'));
        expect(actions[1]).toEqual(setError(errorObj))
    })
})