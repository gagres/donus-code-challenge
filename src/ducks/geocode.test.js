import reducer, {
    Types,
    saveAddress,
    cacheLoad,
    setError,
    geocodeRequestAction,
    initLocation
} from './geocode'
import {
    initRequest, setRequestResponse, setError as setRequestError
} from './request'
import {
    addNotification
} from './notification'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { geocodeRequest } from '../utils/geocode'

const mockStore = configureStore([thunk]);

describe('Geocode Actions', () => {
    const payload = { location: 'hello world' };
    test('save address', () => {
        const result = { type: Types.SAVE_ADDRESS, payload }
        expect(saveAddress(payload)).toEqual(result)
    })
    test('cache load', () => {
        const result = { type: Types.CACHE_LOAD, payload }
        expect(cacheLoad(payload)).toEqual(result)
    })
    test('set error', () => {
        const result = { type: Types.ERROR, payload: { message: payload } }
        expect(setError(payload)).toEqual(result)
    })
})

describe('Geocode Reducer', () => {    
    test('return init state', () => {
        expect(reducer(undefined, {})).toEqual({
            address: null,
            isEmpty: true,
            error: null,
            cacheLoaded: false,
        })
    })
    
    test('set error', () => {
        const payload = { message: 'error', error: 'abc' };
        const initState = {
            address: 'abc',
            isEmpty: true,
            error: null,
            cacheLoaded: true,
        }
        const action = { type: Types.ERROR, payload }
        expect(reducer(initState, action)).toEqual({
            ...initState,
            error: payload
        })
    })

    test('save address', () => {
        const payload = 'address';
        const initState = {
            address: null,
            isEmpty: true,
            error: 'abc',
            cacheLoaded: false,
        }
        const action = { type: Types.SAVE_ADDRESS, payload }
        expect(reducer(initState, action)).toEqual({
            ...initState,
            address: payload,
            isEmpty: false,
            error: null
        });
    })

    test('cache load', () => {
        const payload = 'address';
        const initState = {
            address: null,
            isEmpty: true,
            error: null,
            cacheLoaded: false,
        }
        const action = { type: Types.CACHE_LOAD, payload }
        expect(reducer(initState, action)).toEqual({
            ...initState,
            address: payload,
            isEmpty: false,
            cacheLoaded: true
        });
    })
})

jest.mock('../utils/cache', () => {
    return {
        getItem: jest.fn(() => 'abc'),
        setItem: jest.fn()
    }
});

jest.mock('../utils/geocode', () => {
    return {
        geocodeRequest: jest.fn()
    }
})

jest.mock('../utils/history', () => {
    return {
        push: jest.fn()
    }
})

describe('Geocode Action Creators', () => {
    let store;
    beforeEach(() => {
        store = mockStore({
            geocode: {
                address: null,
                isEmpty: true,
                error: null,
                cacheLoaded: false,
            }
        })
    })

    test('load cached location', () => {
        store.dispatch(initLocation())

        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: Types.CACHE_LOAD,
            payload: 'abc'
        })
    })

    test('geocode request success', () => {
        const address = 'abc';
        const response = {
            body: [
                { geometry: { location: 'abc' } }
            ]
        };
        geocodeRequest.mockReturnValue(Promise.resolve(response));

        return store.dispatch(geocodeRequestAction(address))
            .then(() => {
                const { geometry: { location } } = response.body[0];
                const newAddress = { address, location };
                const actions = store.getActions();
                expect(actions[0]).toEqual(initRequest());
                expect(actions[1]).toEqual(setRequestResponse(response.body));
                expect(actions[2]).toEqual(saveAddress(newAddress))
            })
    })

    test('geocode request fail', () => {
        const address = 'abc';
        const response = {
            error: 'abc'
        }
        geocodeRequest.mockReturnValue(Promise.resolve(response));

        return store.dispatch(geocodeRequestAction(address))
            .then(() => {
                const actions = store.getActions();
                expect(actions[0]).toEqual(initRequest());
                expect(actions[1]).toEqual(addNotification(response.error, 'error'))
                expect(actions[2]).toEqual(setRequestError(response));
                expect(actions[3]).toEqual(setError(response.error));
            })
    })
})