import React from 'react'
import { shallow } from 'enzyme'
import { GlobalLoader } from '.'
import { LoaderText } from './styled'

function shallowSetup(props = {
    loading: false
}) {
    return shallow(<GlobalLoader {...props}/>)
}

describe('GlobalLoader', () => {
    test('renders global loader', () => {
        const wrapper = shallowSetup();
        expect(wrapper.exists()).toBe(true);
    })

    test('not render global loader if loading is false', () => {
        const wrapper = shallowSetup();
        expect(wrapper.html()).toBe(null);
    })

    test('render global loader when loading is true', () => {
        const wrapper = shallowSetup({ loading: true });
        expect(wrapper.find(LoaderText).exists()).toBe(true);
        expect(wrapper).toMatchSnapshot();
    })
})