import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import * as S from './styled'

export const GlobalLoader = ({ loading }) => {
    if(!loading) {
        return null;
    }

    return (
        <S.GlobalLoaderWrapper>
            <S.LoaderContent>
                <S.LoaderText>Aguarde um momento...</S.LoaderText>
            </S.LoaderContent>
        </S.GlobalLoaderWrapper>
    )
}

const mapStateToProps = state => {
    return {
        loading: state.request.loading
    }
}

GlobalLoader.propTypes = {
    loading: PropTypes.bool.isRequired
}

export default connect(mapStateToProps)(GlobalLoader);