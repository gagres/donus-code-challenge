import styled from 'styled-components'

export const GlobalLoaderWrapper = styled.div`
    position: fixed;
    top: 0px;
    left: 0px;
    width: 100%;
    height: 100vh;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 105;
`

export const LoaderContent = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`

export const LoaderText = styled.span`
    font-size: 16px;
    font-family: 'Roboto Medium';
    color: var(--font-yellow);
`