// Jest Snapshot v1, https://goo.gl/fbAQLP

exports[`GlobalLoader render global loader when loading is true 1`] = `
<styled__GlobalLoaderWrapper>
  <styled__LoaderContent>
    <styled__LoaderText>
      Aguarde um momento...
    </styled__LoaderText>
  </styled__LoaderContent>
</styled__GlobalLoaderWrapper>
`;
