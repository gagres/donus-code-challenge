import React, { useEffect, useRef } from 'react'
import PropTypes from 'prop-types'

import * as S from './styed'

const NotificationItem = ({
    notification: { message, type, id },
    timeoutCb
}) => {
    const notifyRef = useRef(null)

    useEffect(() => {
        notifyRef.current.style = 'opacity: 1; transform: translateY(0px);' // animate
        const schedule = setTimeout(() => {
            timeoutCb()
        }, 2000);
        return () => clearTimeout(schedule)
    }, [id])

    return (
        <S.NotificationItemWrapper
            ref={notifyRef}
            type={type}>
            {message}
        </S.NotificationItemWrapper>
    )
}

NotificationItem.propTypes = {
    notification: PropTypes.object.isRequired,
    timeoutCb: PropTypes.func.isRequired,
}

export default NotificationItem