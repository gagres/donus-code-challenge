import styled from 'styled-components'

const colors = {
    warn: '#fec771',
    error: '#eb7070',
    success: '--bg-green'
}

export const NotificationItemWrapper = styled.p`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: auto;
    padding: 20px;
    background-color: #8fbbaf;
    font-size: 14px;
    font-family: 'Roboto Medium';
    text-align: center;
    border-radius: 8px;
    box-shadow: rgba(0, 0, 0, 0.08) 0px 2px 4px 0px;
    opacity: 0;

    transform: translateY(10px);

    &:not(:first-child) {
        margin-top: 5px;
    }

    transition: opacity 0.5s, transform 0.5s;
`