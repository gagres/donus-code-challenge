import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {
    removeNotification as removeNotificationAction
} from '../../ducks/notification'
import NotificationItem from '../NotificationItem'

import * as S from './styled'

export const Notification = ({ notifications, removeNotification }) => {
    function timeout() {
        removeNotification()
    }
    
    return (
        <S.NotificationWrapper>
            {notifications.reverse().map((notification, index) => (
                <NotificationItem key={index}
                    notification={notification}
                    timeoutCb={timeout} />
            ))}
        </S.NotificationWrapper>
    )
}

const mapStateToProps = state => {
    return {
        notifications: state.notification.queue
    }
}

const mapDispatchToProps = dispatch => {
    return bindActionCreators({
        removeNotification: removeNotificationAction
    }, dispatch)
}

Notification.propTypes = {
    notifications: PropTypes.arrayOf(PropTypes.object)
}

export default connect(mapStateToProps, mapDispatchToProps)(Notification)