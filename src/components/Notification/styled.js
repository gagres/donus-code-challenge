import styled from 'styled-components'

export const NotificationWrapper = styled.div`
    width: 100%;
    max-width: 350px;
    height: auto;
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    justify-content: flex-end;
    position: fixed;
    bottom: 10px;
    right: 10px;
    z-index: 100;
`