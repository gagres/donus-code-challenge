import React from 'react'
import PropTypes from 'prop-types'
import {
    formatCurrency
} from '../../utils/formatters'

import imageNotFound from '../../assets/img/image-not-found.png'
import * as S from './styled'

const ProductItem = ({
    product: {
        title,
        productVariants: [{ price }],
        images: [{ url }]
    }
}) => {

    function replaceBrokenSrc(e) {
        e.target.src = imageNotFound;
    }

    return (
        <S.ItemWrapper>
            <S.ItemImage src={url}
                onError={replaceBrokenSrc}/>
            <S.ItemLine />
            <S.ItemInfo>
                <S.ItemTitle>{title}</S.ItemTitle>
                <S.ItemPrice>{formatCurrency(price)}</S.ItemPrice>
            </S.ItemInfo>
            <S.ItemLink>
                Mais Detalhes
            </S.ItemLink>
        </S.ItemWrapper>
    )
}

ProductItem.propTypes = {
    product: PropTypes.object.isRequired
}

export default ProductItem;