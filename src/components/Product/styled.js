import styled from 'styled-components'
import media from 'styled-media-query'

export const ItemWrapper = styled.div`
    width: 180px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    text-align: center;
    margin-left: 15px;
    margin-top: 15px;
    border: 1px solid rgb(255, 255, 255);
    border-radius: 8px;
    box-shadow: rgba(0, 0, 0, 0.08) 0px 2px 4px 0px;

    ${media.lessThan('medium')`
        width: 160px;
    `}
`

export const ItemImage = styled.img`
    width: 40%;
    height: auto;
    align-self: center;
    min-height: 70px;
    max-height: 80px;
`

export const ItemLine = styled.hr`
    margin: 0;
    width: 100%;
    height: 0px;
    overflow: visible;
    border: 1px solid rgb(243,243,243);
`

export const ItemInfo = styled.div`
    margin-top: 10px;
    padding: 5px;
    text-align: left;
`

export const ItemTitle = styled.h3`
    text-align: left;
    color: var(--font-grey);
    overflow: hidden;
    text-overflow: ellipsis;
    font-size: 14px;
`

export const ItemPrice = styled.p`
    margin-top: 20px;
    font-size: 16px;
    font-family: 'Roboto Thin';
    text-align: left;
`

export const ItemLink = styled.a`
    font-size: 14px;
    font-family: 'Roboto Medium';
    margin-top: 10px;
    padding: 5px;
    text-align: left;
    text-transform: uppercase;
`