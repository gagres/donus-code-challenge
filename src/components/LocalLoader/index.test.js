import React from 'react'
import { shallow } from 'enzyme'
import LocalLoader from '.'
import { LocalLoaderText } from './styled'

function shallowSetup(props = {
    text: null
}) {
    return shallow(<LocalLoader {...props}/>)
}

describe('LocalLoader', () => {
    test('render local loader', () => {
        const wrapper = shallowSetup();
        expect(wrapper.exists()).toBe(true);
    })
    
    test('render local loader with default text', () => {
        const wrapper = shallowSetup();
        expect(wrapper.exists()).toBe(true);
        expect(wrapper.find(LocalLoaderText).text()).toBe('Carregando');
    })

    test('render local loader with given text', () => {
        const text = 'other';
        const wrapper = shallowSetup({ text });
        expect(wrapper.find(LocalLoaderText).text()).toBe(text);
    })
})