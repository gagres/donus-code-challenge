import React from 'react'
import PropTypes from 'prop-types'

import * as S from './styled'

const LocalLoader = ({ text }) => (
    <S.LocalLoaderWrapper>
        <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
        <S.LocalLoaderText>{text || 'Carregando'}</S.LocalLoaderText>
    </S.LocalLoaderWrapper>
)

LocalLoader.propTypes = {
    text: PropTypes.string
}

export default LocalLoader;