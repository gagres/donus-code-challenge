import styled from 'styled-components'

export const LocalLoaderWrapper = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`

export const LocalLoaderText = styled.span`
    margin-top: 20px;
    font-size: 16px;
    font-family: 'Roboto Medium';
    color: var(--font-black);
`