import styled from 'styled-components'
import media from 'styled-media-query'

export const ProductListWrapper = styled.div`
    margin-top: 2.5%;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;

    ${media.lessThan('medium')`
        margin-top: 10%;
    `}
`

export const ProductsTitle = styled.h1`
    margin-top: 20px;
    font-size: 32px;
    color: var(--font-black);
    font-family: 'Roboto Black';
    
    ${media.lessThan('medium')`
        text-align: center;
    `}
`