import React from 'react'
import PropTypes from 'prop-types'
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

import ProductItem from '../Product'
import LocalLoader from '../LocalLoader'
import Message from '../Message'

import * as S from './styled'

const GET_PRODUCTS = gql`
    query poc($id: ID!, $categoryId: Int, $search: String){
        poc(id: $id) {
            id
            products(categoryId: $categoryId, search: $search) {
                id
                title
                rgb
                images {
                url
                }
                productVariants {
                availableDate
                productVariantId
                price
                inventoryItemId
                shortDescription
                title
                published
                volume
                volumeUnit
                description
                subtitle
                }
            }
        }
    }
`

const ProductList = ({ pocId }) => {
    return (
        <Query query={GET_PRODUCTS}
            variables={{ id: pocId }}>
            {
                ({ data, loading, error }) => {
                    if (loading) {
                        return (
                            <LocalLoader text="Carregando produtos..."/>
                        )
                    }
                    
                    if (error) {
                        return (
                            <Message text="Ocorreu um erro ao carregar os produtos =("
                                linkTitle="Voltar ao início"
                                linkHref="/"/>
                        )
                    }

                    if (!data.poc.products) {
                        return (
                            <Message text="Não encontramos nenhum produto para este endereço =]"
                                linkTitle="Buscar endereços"
                                linkHref="/"/>
                        )
                    }

                    return (
                        <>
                            <S.ProductsTitle>Delivery de Bebidas</S.ProductsTitle>
                            <S.ProductListWrapper>
                                {data.poc.products.map(product => (
                                    <ProductItem key={product.id}
                                        product={product}/>
                                ))}
                            </S.ProductListWrapper>
                        </>
                    )
                }
            }
        </Query>
    )
}

ProductList.propTypes = {
    pocId: PropTypes.string
}

export default ProductList;