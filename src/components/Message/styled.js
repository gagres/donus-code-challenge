import styled from 'styled-components'
import { Link } from 'react-router-dom'

export const MessageWrapper = styled.section`
    width: 100%;
    height: 70vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    font-size: 18px;
    font-family: 'Roboto Medium';
    color: var(--font-grey);
`

export const MessageButton = styled(Link)`
    width: 100%;
    max-width: 300px;
    text-align: center;
    text-decoration: none;
    color: var(--font-black);
    font-size: 14px;
    font-family: 'Roboto Medium';
    text-transform: uppercase;
    line-height: 20px;
    background-color: var(--bg-yellow);
    border-radius: 24px;
    border: none;
    padding: 12px;
    margin-top: 20px;
    cursor: pointer;

    &:hover {
        background-color: var(--bg-yellow-hover);
    }
`