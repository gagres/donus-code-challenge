import React from 'react'
import { shallow } from 'enzyme'

import Message from '.'
import { MessageButton } from './styled'

function shallowSetup(props = {
    text: 'teste',
}) {
    return {
        wrapper: shallow(
            <Message {...props}/>
        )
    }
}

describe('Message Component', () => {
    test('render message component', () => {
        const { wrapper } = shallowSetup();
        expect(wrapper.exists()).toBe(true);
    })

    test('use props passed to component', () => {
        const props = {
            text: 'texto de teste',
            linkHref: '/',
            linkTitle: 'button'
        };
        const { wrapper } = shallowSetup(props);

        expect(wrapper.find('p').text()).toBe(props.text);
        expect(wrapper.find(MessageButton).text()).toBe(props.linkTitle);
        expect(wrapper.find(MessageButton).prop('to')).toBe(props.linkHref);
    })

    test('snapshot of a default message and home link', () => {
        const props = {
            text: 'texto de teste',
            linkHref: '/',
            linkTitle: 'button'
        };
        const { wrapper } = shallowSetup(props);

        expect(wrapper).toMatchSnapshot();
    })
})