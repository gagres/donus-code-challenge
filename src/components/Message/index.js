import React from 'react'
import PropTypes from 'prop-types'

import * as S from './styled'

const Message = ({ text, linkTitle, linkHref }) => (
    <S.MessageWrapper>
        <p>{text}</p>
        {linkTitle && linkHref && (
            <S.MessageButton to={linkHref}>
                {linkTitle}
            </S.MessageButton>
        )}
    </S.MessageWrapper>
)

Message.propTypes = {
    text: PropTypes.string.isRequired,
    linkTitle: PropTypes.string,
    linkHref: PropTypes.string
}

export default Message;