import styled from 'styled-components'
import { Link } from 'react-router-dom'

export const HeaderNav = styled.nav`
    position: fixed;
    top: 0;
    left: 0;
    background-color: #333;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: var(--header-height);
    padding: 10px;
    z-index: 10;
`

export const HeaderNavWrapper = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    max-width: 1200px;
`

export const HeaderItemGroup = styled.div`
    display: block;
    display: flex;
    align-items: center;
`

export const HeaderItem = styled.div`
    max-width: 200px;
    
    &:not(:first-child) {
        margin-left: 40px;
    }
`

export const HeaderLink = styled(Link)`
    display: block;
    text-decoration: none;
`

export const HeaderIcon = styled.img`
    display: block;
    height: auto;
    width: 50px;
    cursor: pointer;
`

export const HeaderIconLg = styled(HeaderIcon)`
    width: 150px;
`

export const HeaderIconSm = styled(HeaderIcon)`
    width: 20px;
`

export const HeaderAddress = styled.div`
    color: var(--font-white);
    font-size: 14px;
`

export const HeaderAddressSub = styled.div`
    color: var(--font-yellow);
    font-size: 14px;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    margin-top: 4px;
`