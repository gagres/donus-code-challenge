// Jest Snapshot v1, https://goo.gl/fbAQLP

exports[`Header render header with address complement 1`] = `
<styled__HeaderNav>
  <styled__HeaderNavWrapper>
    <styled__HeaderItemGroup>
      <styled__HeaderItem>
        <styled__HeaderLink
          to="/"
        >
          <styled__HeaderIcon
            src="test-file-stub"
          />
        </styled__HeaderLink>
      </styled__HeaderItem>
      <styled__HeaderItem>
        <styled__HeaderLink
          to="/produtos"
        >
          <styled__HeaderAddress>
            Receber agora em
          </styled__HeaderAddress>
          <styled__HeaderAddressSub>
            Testando
          </styled__HeaderAddressSub>
        </styled__HeaderLink>
      </styled__HeaderItem>
    </styled__HeaderItemGroup>
  </styled__HeaderNavWrapper>
</styled__HeaderNav>
`;

exports[`Header render header without address complement 1`] = `
<styled__HeaderNav>
  <styled__HeaderNavWrapper>
    <styled__HeaderItemGroup>
      <styled__HeaderItem>
        <styled__HeaderLink
          to="/"
        >
          <styled__HeaderIconLg
            src="test-file-stub"
          />
        </styled__HeaderLink>
      </styled__HeaderItem>
    </styled__HeaderItemGroup>
  </styled__HeaderNavWrapper>
</styled__HeaderNav>
`;
