import React from 'react'
import PropTypes from 'prop-types'
import RouterPropTypes from 'react-router-prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import * as S from './styled'

import logoLarge from '../../assets/img/logo-lg.png'
import logoSmall from '../../assets/img/logo.png'

export const Header = ({ address, location }) => {
    const { pathname } = location;

    return (
        <S.HeaderNav>
            <S.HeaderNavWrapper>
                <S.HeaderItemGroup>
                    {(!address || pathname === '/') && (
                        <S.HeaderItem>
                            <S.HeaderLink to="/">
                                <S.HeaderIconLg src={logoLarge}/>
                            </S.HeaderLink>
                        </S.HeaderItem>
                    )}
                    {(pathname !== '/' && address) && (
                        <S.HeaderItem>
                            <S.HeaderLink to="/">
                                <S.HeaderIcon src={logoSmall}/>
                            </S.HeaderLink>
                        </S.HeaderItem>
                    )}
                    {address && (
                        <S.HeaderItem>
                            <S.HeaderLink to="/produtos">
                                <S.HeaderAddress>Receber agora em</S.HeaderAddress>
                                <S.HeaderAddressSub>{address.address}</S.HeaderAddressSub>
                            </S.HeaderLink>
                        </S.HeaderItem>
                    )}
                </S.HeaderItemGroup>
            </S.HeaderNavWrapper>
        </S.HeaderNav>
    )
}

const mapStateToProps = state => ({
    address: state.geocode.address
})

Header.propTypes = {
    address: PropTypes.object,
    location: RouterPropTypes.location
}

export default withRouter(connect(mapStateToProps)(Header));