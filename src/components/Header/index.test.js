import React from 'react'
import { shallow } from 'enzyme'
import { Header } from '.'
import { HeaderAddress } from './styled'

function shallowSetup(props = {
    address: null,
    location: {
        pathname: '/',
        search: '',
        hash: ''
    }
}) {
    return shallow(<Header address={props.address}
        location={props.location}/>);
}

describe('Header', () => {
    test('render header without address complement', () => {
        const wrapper = shallowSetup();
        expect(wrapper.find(HeaderAddress).exists()).toBe(false);
        expect(wrapper).toMatchSnapshot();
    })
  
    test('render header with address complement', () => {
        const address = {
            address: 'Testando'
        };
        const wrapper = shallowSetup({
            address,
            location: {
                pathname: '/produtos',
                search: '',
                hash: ''
            }
        });

        expect(wrapper.find(HeaderAddress).exists()).toBe(true);
        expect(wrapper).toMatchSnapshot();
    })
})