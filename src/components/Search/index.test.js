import React from 'react'
import { shallow } from 'enzyme'
import Search from './index'

function shallowSetup(props = {
    value: '',
    onChange: jest.fn()
}) {
    return shallow(<Search {...props}/>);
}

describe('Search', () => {
    const initValue = "Rua Tapajós, 215";
    const onChange = jest.fn();
    const search = shallowSetup({ value: initValue, onChange })

    test('renders the Search', () => {
        expect(search.exists()).toBe(true)
    })

    test('should use props value', () => {
        expect(search.prop('value')).toBe(initValue)
    })

    test('should use props value', () => {
        const value = { target: { value: "abcde" } };
        search.simulate('change', value)
        search.simulate('keydown', { keyCode: 13 })

        expect(onChange.mock.calls.length).toBe(1)
        expect(onChange.mock.calls[0][0]).toBe(value)
    })
})