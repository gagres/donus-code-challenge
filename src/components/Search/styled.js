import styled from 'styled-components'

export const Input = styled.input`
    width: 100%;
    height: 48px;
    font-family: 'Roboto';
    font-size: 16px;
    font-weight: lighter;
    line-height: 20px;
    background-color: #FFFFFF;
    color: var(--font-black);
    border-radius: 8px;
    border: 1px solid rgb(153, 153, 153);
    padding-left: 30px;
`