import React from 'react'
import PropTypes from 'prop-types'

import * as S from './styled'

const Search = ({ value, onChange }) => (
    <S.Input type="text"
        value={value}
        onChange={onChange}
        placeholder="Insira o endereço com número"/>
)

Search.propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired
}

export default Search;