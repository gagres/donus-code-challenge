import React from 'react'
import PropTpes from 'prop-types'

import Header from '../Header'
import Footer from '../Footer'
import Notification from '../Notification'
import GlobalLoader from '../GlobalLoader'
import * as S from './styled'

const ContentWrapper = ({ children }) => (
    <>
        <Header />
        <S.ContentWrapper>
            {children}
        </S.ContentWrapper>
        <Footer />
        <Notification />
        <GlobalLoader />
    </>
)

ContentWrapper.propTypes = {
    children: PropTpes.object.isRequired
}

export default ContentWrapper
