import styled from 'styled-components'

export const ContentWrapper = styled.section`
    margin-top: var(--header-height);
    margin-bottom: 60px;
    width: 100%;
    height: auto;
    min-height: 70vh;
    display: flex;
    justify-content: center;
`