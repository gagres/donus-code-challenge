import React from 'react'

import * as S from './styled'

const Footer = () => (
    <S.FooterWrapper>
        <S.FooterGoBack>
            <S.FooterGoBackLink href="#">Voltar ao topo</S.FooterGoBackLink>
        </S.FooterGoBack>
        <S.FooterBottom>
            <S.FooterSpan>@2020 Gabriel - todos os direitos reservados</S.FooterSpan>
        </S.FooterBottom>
    </S.FooterWrapper>
)

export default Footer;