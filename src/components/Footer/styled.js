import styled from 'styled-components'
import media from 'styled-media-query'

export const FooterWrapper = styled.section`
    width: 100%;
    background-color: var(--bg-black);
`

export const FooterGoBack = styled.div`
    width: 100%;
    height: 56px;
    border: none;
    background-color: var(--font-grey-2);
    display: flex;
    justify-content: center;
    align-items: center;
`

export const FooterGoBackLink = styled.a`
    text-decoration: none;
    color: var(--font-white);
    font-weight: 500;
    font-size: 14px;
    cursor: pointer;

    ${media.lessThan('medium')`
        font-size: 16px;
    `}
`

export const FooterBottom = styled.div`
    width: 100%;
    padding: 12px 20px;
    display: flex;
    justify-content: center;
    align-items: center;
`

export const FooterSpan = styled.span`
    color: var(--font-white);
    font-weight: bold;
    font-size: 12px;
`