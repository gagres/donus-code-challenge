import React from 'react'
import ReactDOM from 'react-dom'
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { HttpLink } from 'apollo-link-http'
import { Provider } from 'react-redux'

// redux store
import store from './store'

import App from './App'
import { GlobalStyling } from './global-style'

const ZE_BASE_URL = process.env.ZE_BASE_URL;
const httpLink = new HttpLink({
    uri: ZE_BASE_URL,
})
const cache = new InMemoryCache()
const client = new ApolloClient({
    link: httpLink,
    cache,
})

ReactDOM.render(
    <div>
        <GlobalStyling />
        <ApolloProvider client={client}>
            <Provider store={store}>
                <App/>
            </Provider>
        </ApolloProvider>
    </div>,
    document.getElementById('root'))