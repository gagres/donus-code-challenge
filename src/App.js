import React from 'react'
import { 
    Router,
    Route,
    Switch
} from 'react-router-dom'
import { connect } from 'react-redux'

import history from './utils/history'
import { initLocation } from './ducks/geocode'

import HomePage from './pages/Home'
import ProductsPage from './pages/Products'
import NotFoundPage from './pages/NotFound'

const App = ({
    initLocation
}) => {
    initLocation();
    return (
        <Router history={history}>
            <Switch>
                <Route exact path="/" component={HomePage}/>
                <Route path="/produtos" component={ProductsPage}/>
                <Route component={NotFoundPage}/>
            </Switch>
        </Router>
    )
}

const mapDispatchToProps = dispatch => {
    return {
        initLocation: () => dispatch(initLocation())
    }
}


export default connect(undefined, mapDispatchToProps)(App);