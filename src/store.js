import { 
    createStore,
    combineReducers,
    applyMiddleware
} from 'redux'
import ReduxThunk from 'redux-thunk'

import geocode from './ducks/geocode'
import notification from './ducks/notification'
import request from './ducks/request'

const rootReducer = combineReducers({
    geocode,
    notification,
    request
})

const store = createStore(rootReducer, applyMiddleware(ReduxThunk))

export default store