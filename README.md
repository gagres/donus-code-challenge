# Donus Code Challenge
This is the repository of donus code challenge front-end project, following the project requirements of [here](https://github.com/ztech-company/donus-code-challenge/blob/master/frontend-mobile.md)

## Running
To run the project, install node dependencies using:
```sh
npm install
```

On the root of the project, run the command above to create env file:
```sh
cp .env-example .env
```
- It's necessary to set Google Geocode API Key on: `.env` file before continue

After that, run:
```sh
npm start
```
The application will start on <http://localhost:8080>
- On the search input, if you use `Rua Américo Brasiliense, São Paulo` it will return a valid product list


## Tests
Tests are built using Jest/Enzyme (for react components). Run the test suite with:
```sh
npm test (-- --watch if you want watch mode)
```
